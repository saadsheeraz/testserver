const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoose = require("mongoose");

//Express Server
const app = express();
const port = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());

const { mogoUrl } = require("./keys");

//Connection with mongo DB
mongoose.connect(mogoUrl, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true,
});

//Conection - Sucess with Mongoose
mongoose.connection.on("connected", () => {
  console.log(
    "Work-Web-Server is connected to Mongo successfully, boo yaaaa!!"
  );
});

//Conection - Failed with Mongoose
mongoose.connection.on("error", (err) => {
  console.log("Error!! work-web-server-connection with Mongo failed", err);
});

const TestFileSchema = require("./testAPI/routes/testFolder/testFile");

require("./testAPI/models/testFolder/testFile");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json({ extended: false }));

//Work-Web-Server Module wise APIs status display
app.use(require("./auth"));

//Use routes via API - http://work-web-server.herokuapp.com/:routes
app.use("/testFolder/testFile", TestFileSchema);

app.listen(port, () => {
  console.log(`Work-Web-Server Started successfully on ${port}`);
});
