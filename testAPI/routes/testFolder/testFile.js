const express = require("express");
const router = express.Router();

router.get("/", (req, res) => res.send("TestFile routes works"));
const TestFileSchema = require("../../models/testFolder/testFile");

router.post("/postData", (req, res) => {
  console.log("data comming", req.body);
  const newTestFile = new TestFileSchema({
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    gender: req.body.gender,
    dateofBirth: req.body.dateofBirth,
    picture: req.body.picture,
    profession: req.body.profession,
    shoeSize: req.body.shoeSize,
    hairColor: req.body.hairColor,
    hairLength: req.body.hairLength,
    braSize: req.body.braSize,
    waistSize: req.body.waistSize,
    height: req.body.height,
    weight: req.body.weight,
    castingType: req.body.castingType,
  })
    .save()
    .then((myRecords) => res.json(myRecords))
    .catch((err) => console.log(err));
});

router.get("/all", (req, res) => {
  TestFileSchema.find()
    .then((deps) => {
      if (!deps) {
        errors.noRecruit = "There are no Programs";
        return res.status(404).json(errors);
      }

      res.json(deps);
    })
    .catch((err) =>
      res.status(404).json({ bulletin: "There are no Programs" })
    );
});

module.exports = router;
